CREATE TABLE 'User' (
    'id' INT NOT NULL AUTO_INCREMENT,
    'username' VARCHAR(150) NOT NULL,
    'first_name' VARCHAR(150) NOT NULL,
    'last_name' VARCHAR(150) NOT NULL,
    'middle_name' VARCHAR(150) NOT NULL,
    'email' VARCHAR(150) NOT NULL,
    'identity_number' VARCHAR(150) NOT NULL,
    'itn' VARCHAR(150) NOT NULL,
    'document_nuber' VARCHAR(150) NOT NULL,
    PRIMARY KEY ('id')
);

CREATE TABLE 'Tender' (
    'id' INT NOT NULL AUTO_INCREMENT,
    'CreaterId' INT NOT NULL,
    'LastVoterId' INT,
    'max_price' INT NOT NULL,
    'name' VARCHAR(255) NOT NULL,
    'full_task' VARCHAR(255) NOT NULL,
    'start_date' DATE,
    'end_date' DATE,
    'deadline' DATE,
    PRIMARY KEY ('id')
);

CREATE TABLE 'Document' (
    'id' INT NOT NULL AUTO_INCREMENT,
    'TenderId' INT NOT NULL,
    'path' VARCHAR(150) NOT NULL UNIQUE,
    PRIMARY KEY ('id')
);

CREATE TABLE 'Vote' (
    'id' INT NOT NULL AUTO_INCREMENT,
    'VoterId' INT NOT NULL,
    'TenderId' INT NOT NULL,
    'vote_at' DATE,
    'price' INT NOT NULL,
    PRIMARY KEY ('id')
);

ALTER TABLE 'Tender' ADD CONSTRAINT 'Tender_fk0' FOREIGN KEY ('CreatertId') REFERENCES 'User'('id');

ALTER TABLE 'Tender' ADD CONSTRAINT 'Tender_fk1' FOREIGN KEY ('LastVoterId') REFERENCES 'User'('id');

ALTER TABLE 'Document' ADD CONSTRAINT 'Document_fk0' FOREIGN KEY ('TenderId') REFERENCES 'Tender'('id');

ALTER TABLE 'Vote' ADD CONSTRAINT 'Vote_fk0' FOREIGN KEY ('VoterId') REFERENCES 'User'('id');

ALTER TABLE 'Vote' ADD CONSTRAINT 'Vote_fk1' FOREIGN KEY ('TenderId') REFERENCES 'Tender'('id');
