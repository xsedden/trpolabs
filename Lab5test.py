#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
import os
import shutil
import subprocess
import tempfile
import unittest
from datetime import datetime


from main.lib.recurrent_task import Recurrent
from main.lib.person import Person
from main.lib.config import load_config, take_logs_path
from main.lib.config import take_text_path
from main.lib.config import take_users_path
from main.lib.logger import init_logger
from main.lib.convert_data import add_task_to_file
from main.lib.convert_data import delete_task_by_id, \
    get_status_by_id, get_task_by_name, \
    from_file_by_path, get_task_by_id, get_person_by_id, get_admin_by_id, \
    get_creater_by_id, get_rec_task_by_id
from main.lib.edit import finish_task, delete_task, edit_deadline, edit_famaly, edit_name, edit_priority, \
    check_deadline
from main.lib.person import add_access_person, remove_access_person, add_access_admin, remove_access_admin
from main.lib.person import login_person
from main.lib.person import get_login_person
from main.lib.recurrent_task import remove_rec, check_rec
from main.lib.task import Task
from main.lib.exceptions import TaskError, UserError, IDError


class UnitTests(unittest.TestCase):
    def setUp(self):
        self.__tracker_dir = tempfile.mkdtemp()
        load_config(self.__tracker_dir, True)

    def tearDown(self):
        shutil.rmtree(self.__tracker_dir)
        subprocess.call(['rm', os.path.join(os.path.expanduser('~/.track'), 'tmp.json')])

    def test_create_task(self):
        task = Task('task', "text.txt" , None, 5, 20181204, 'Misha')
        add_task_to_file(task)
        users_dict = get_task_by_name('task')
        text2 = os.path.join(self.__tracker_dir, 'text')
        text2 = os.path.join(text2, 'text.txt')
        self.assertEqual(users_dict["text"], text2)
        self.assertEqual(users_dict["famaly"], None)
        self.assertEqual(users_dict["priority"], 5)
        self.assertEqual(users_dict["deadline"], 20181204)
        self.assertEqual(users_dict["persons"]["creater"], 'Misha')
        self.assertEqual((users_dict["persons"]["person"]).pop(), 'Misha')

    def test_create_user(self):
        pers = Person('login', 'password')
        pers.create_person()
        content = [{'login': 'login', 'password': 'password'}]
        users_dict = from_file_by_path(take_users_path())
        self.assertEqual(users_dict, content)

    def test_authorization(self):
        pers = Person('login', 'password')
        pers.create_person()
        login_person("login", "password")
        self.assertEqual(get_login_person(), 'login')

    def test_edit_head_task(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task', None, None, 5, 20181204, 'Misha')
        add_task_to_file(task)
        task2 = Task('task2', None, None, 6, 20181001, 'Misha')
        add_task_to_file(task2)
        finish_task(1)
        edit_name(1, 'Newname')
        edit_priority(1, 3)
        edit_deadline(1, 20180101)
        edit_famaly(1, 2)
        content = {'task_id': 1, 'name': 'Newname', 'text': None, 'famaly': 2,
                   'status': 1,
                   'priority': 3, 'createdate': datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S"),
                   'deadline': 20180101,
                   "persons": {'creater': 'Misha', 'admin': ['Misha'], 'person': ['Misha']}}
        self.assertEqual(get_task_by_id(1), content)

    def test_remove_head_task(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task', None, None, 5, 20181204, 'Misha')
        add_task_to_file(task)
        delete_task_by_id(1)
        self.assertFalse(get_task_by_id(1))

    def test_remove_head_task_with_subs(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task', None, None, 5, 20181204, 'Misha')
        add_task_to_file(task)
        task2 = Task('task2', None, 1, 6, 20181001, 'Misha')
        add_task_to_file(task2)
        delete_task(1)
        self.assertFalse(get_task_by_id(1))
        self.assertFalse(get_task_by_id(2))

    def test_finish_task_with_subs(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task', None, None, 5, 20181204, 'Misha')
        add_task_to_file(task)
        task2 = Task('task2', None, 1, 6, 20181001, 'Misha')
        add_task_to_file(task2)
        finish_task(1)
        self.assertEqual(get_status_by_id(1), 1)
        self.assertEqual(get_status_by_id(2), 1)

    def test_deadline_task(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task2', None, None, 6, 20180101, 'Misha')
        add_task_to_file(task)
        task2 = Task('task1', None, None, 6, 20180101, 'Misha')
        add_task_to_file(task2)
        check_deadline()
        finish_task(2)
        self.assertEqual(get_status_by_id(1), 2)
        self.assertEqual(get_status_by_id(2), 1)

    def test_add_access_person(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task2', None, None, 6, 20180101, 'Misha')
        add_task_to_file(task)
        pers2 = Person('login', 'password')
        pers2.create_person()
        add_access_person('login', 1)
        self.assertEqual(get_person_by_id(1), ['Misha', 'login'])
        self.assertEqual(get_admin_by_id(1), ['Misha'])
        self.assertEqual(get_creater_by_id(1), 'Misha')

    def test_add_access_admin(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task2', None, None, 6, 20180101, 'Misha')
        add_task_to_file(task)
        pers2 = Person('login', 'password')
        pers2.create_person()
        add_access_admin('login', 1)
        self.assertEqual(get_person_by_id(1), ['Misha', 'login'])
        self.assertEqual(get_admin_by_id(1), ['Misha', 'login'])
        self.assertEqual(get_creater_by_id(1), 'Misha')

    def test_add_access_person_rec(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task2', None, None, 6, 20180101, 'Misha')
        add_task_to_file(task)
        task2 = Task('task1', None, 1, 6, 20180101, 'Misha')
        add_task_to_file(task2)
        pers2 = Person('login', 'password')
        pers2.create_person()
        add_access_person('login', 1)
        self.assertEqual(get_person_by_id(1), ['Misha', 'login'])
        self.assertEqual(get_admin_by_id(1), ['Misha'])
        self.assertEqual(get_creater_by_id(1), 'Misha')
        self.assertEqual(get_person_by_id(2), ['Misha', 'login'])
        self.assertEqual(get_admin_by_id(2), ['Misha'])
        self.assertEqual(get_creater_by_id(2), 'Misha')

    def test_add_access_admin_rec(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task2', None, None, 6, 20180101, 'Misha')
        add_task_to_file(task)
        task2 = Task('task1', None, 1, 6, 20180101, 'Misha')
        add_task_to_file(task2)
        pers2 = Person('login', 'password')
        pers2.create_person()
        add_access_admin('login', 1)
        self.assertEqual(get_person_by_id(1), ['Misha', 'login'])
        self.assertEqual(get_admin_by_id(1), ['Misha', 'login'])
        self.assertEqual(get_creater_by_id(1), 'Misha')
        self.assertEqual(get_person_by_id(2), ['Misha', 'login'])
        self.assertEqual(get_admin_by_id(2), ['Misha', 'login'])
        self.assertEqual(get_creater_by_id(2), 'Misha')

    def test_remove_access_admin_rec(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task2', None, None, 6, 20180101, 'Misha')
        add_task_to_file(task)
        task2 = Task('task1', None, 1, 6, 20180101, 'Misha')
        add_task_to_file(task2)
        pers2 = Person('login', 'password')
        pers2.create_person()
        add_access_admin('login', 1)
        remove_access_admin('login', 1)
        self.assertEqual(get_person_by_id(1), ['Misha', 'login'])
        self.assertEqual(get_admin_by_id(1), ['Misha'])
        self.assertEqual(get_creater_by_id(1), 'Misha')
        self.assertEqual(get_person_by_id(2), ['Misha', 'login'])
        self.assertEqual(get_admin_by_id(2), ['Misha'])
        self.assertEqual(get_creater_by_id(2), 'Misha')

    def test_remove_person_admin_rec(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task2', None, None, 6, 20180101, 'Misha')
        add_task_to_file(task)
        task2 = Task('task1', None, 1, 6, 20180101, 'Misha')
        add_task_to_file(task2)
        pers2 = Person('login', 'password')
        pers2.create_person()
        add_access_admin('login', 1)
        remove_access_person('login', 1)
        self.assertEqual(get_person_by_id(1), ['Misha'])
        self.assertEqual(get_admin_by_id(1), ['Misha'])
        self.assertEqual(get_creater_by_id(1), 'Misha')
        self.assertEqual(get_person_by_id(2), ['Misha'])
        self.assertEqual(get_admin_by_id(2), ['Misha'])
        self.assertEqual(get_creater_by_id(2), 'Misha')

    def test_add_recurrent_task(self):
        pers = Person('login', 'password')
        pers.create_person()
        login_person("login", "password")
        rec = Recurrent('d', None, None, 'name', 20180701)
        rec.add_rec()
        content = {
            'rec_id': 1,
            'typetask': 'd',
            'text': None,
            'famaly': None,
            'createdate': datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S"),
            'name': 'name',
            'nextdate': 20180701,
            "priority": 10,
            "creater": get_login_person()
        }
        self.assertEqual(get_rec_task_by_id(1), content)

    def test_remove_recurrent_task(self):
        pers = Person('login', 'password')
        pers.create_person()
        login_person("login", "password")
        rec = Recurrent('d', None, None, 'name', 20180701)
        rec.add_rec()
        remove_rec(1)
        self.assertEqual(get_rec_task_by_id(1), None)

    def test_before_start_date_recurrent_task(self):
        pers = Person('login', 'password')
        pers.create_person()
        login_person("login", "password")
        rec = Recurrent('d', None, None, 'name', 20181001)
        rec.add_rec()
        check_rec()
        self.assertEqual(get_task_by_id(1), None)

    def test_after_start_date_recurrent_task(self):
        pers = Person('login', 'password')
        pers.create_person()
        login_person("login", "password")
        rec = Recurrent('w', None, None, 'name', 20180623)
        rec.add_rec()
        check_rec()
        content = {
            'task_id': 1, 'name': 'name', 'text': None, 'famaly': None,
            'status': None,
            'priority': 10, 'createdate': datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S"), 'deadline': 20180623,
            "persons": {'creater': get_login_person(), 'admin': [get_login_person()], 'person': [get_login_person()]}
        }
        content2 = {
            'task_id': 2, 'name': 'name', 'text': None, 'famaly': None,
            'status': None,
            'priority': 10, 'createdate': datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S"), 'deadline': 20180630,
            "persons": {'creater': get_login_person(), 'admin': [get_login_person()], 'person': [get_login_person()]}
        }
        self.assertEqual(get_task_by_id(1), content)
        self.assertEqual(get_task_by_id(2), content2)
        self.assertEqual(get_task_by_id(3), None)

    def test_after_start_date_m_recurrent_task(self):
        pers = Person('login', 'password')
        pers.create_person()
        login_person("login", "password")
        rec = Recurrent('m', None, None, 'name', 20180531)
        rec.add_rec()
        check_rec()
        content = {
            'task_id': 1, 'name': 'name', 'text': None, 'famaly': None,
            'status': None,
            'priority': 10, 'createdate': datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S"), 'deadline': 20180531,
            "persons": {'creater': get_login_person(), 'admin': [get_login_person()], 'person': [get_login_person()]}
        }
        content2 = {
            'task_id': 2, 'name': 'name', 'text': None, 'famaly': None,
            'status': None,
            'priority': 10, 'createdate': datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S"), 'deadline': 20180630,
            "persons": {'creater': get_login_person(), 'admin': [get_login_person()], 'person': [get_login_person()]}
        }
        self.assertEqual(get_task_by_id(1), content)
        self.assertEqual(get_task_by_id(2), content2)
        self.assertEqual(get_task_by_id(3), None)




    def test_add_access_admin_fail(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        login_person("Misha", "password")
        task = Task('task2', None, None, 6, 20180101, 'Mishsdfsa2')
        add_task_to_file(task)
        pers2 = Person('login', 'password')
        pers2.create_person()
        with self.assertRaises(UserError):
            add_access_admin('login', 1)

    def test_edit_nonexistent_task(self):
        with self.assertRaises(IDError):
            edit_name(1, 'Newname')

    def test_remove_nonexistent_task(self):
        with self.assertRaises(IDError):
            delete_task(1)

    def test_remove_nonexistent_rec_task(self):
        with self.assertRaises(IDError):
            remove_rec(1)


    def test_authorization_fail(self):
        pers = Person('Misha', 'password')
        pers.create_person()
        with self.assertRaises(UserError):
            login_person("login", "password")
